const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: './',
  outputDir: 'publicSystem', // 打包后的项目目录名称
  devServer: {
    publicPath: '/',
    port: '8080'
  },
  chainWebpack: config => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('assets', resolve('src/assets'))
      .set('components', resolve('src/components'))
      .set('@serviceApp', resolve('src/views/serviceApp'))
  }
}
