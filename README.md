# guangdong
```
广电5G业务运营支撑系统
```
## 目录介绍
```
src/assets/images:图片
src/components/container:容器组件
src/components/layout:布局组件
视图页面:src/views/
homePage:首页
customerPage:客户首页
accountManageChange:客户经理变更申请
integrationBusiness:5G融合业务
addressManage:标准地址管理
managerChangeReview:客户经理变更审核
各页面基础样式:src/styles/css/base
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
