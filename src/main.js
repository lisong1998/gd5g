import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/styles/css/index.css'
import '@/styles/css/base/base.scss'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.config.productionTip = false
Vue.prototype.$echarts = require('echarts')
Vue.use(ElementUI)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
