import Vue from 'vue'
import VueRouter from 'vue-router'
import common from '@/router/common'
import system from '@/router/system'
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [...common, ...system]
})

export default router
