/**
 * 系统路由
 */
export default [{
  path: '/system',
  name: 'System',
  component: () =>
    import(/* webpackChunkName: "Layout" */ '@/components/layout/Layout'),
  children: [
    {
      path: '/',
      name: 'HomePage',
      meta: '首页',
      component: () =>
        import(/* webpackChunkName: "districtManage" */ '@/views/serviceApp/homePage/Index')
    },
    {
      path: 'customerPage',
      name: 'CustomerPage',
      meta: '客户首页',
      component: () =>
        import(/* webpackChunkName: "districtManage" */ '@/views/serviceApp/customerPage/Index')
    },
    {
      path: 'accountManageChange',
      name: 'AccountManageChange',
      meta: '客户经理变更申请',
      component: () =>
        import(/* webpackChunkName: "districtManage" */ '@/views/serviceApp/accountManageChange/Index')
    },
    {
      path: 'integrationBusiness',
      name: 'IntegrationBusiness',
      meta: '5G融合业务',
      component: () =>
        import(/* webpackChunkName: "districtManage" */ '@/views/serviceApp/integrationBusiness/Index')
    },
    {
      path: 'addressManage',
      name: 'AddressManage',
      meta: '标准地址管理',
      component: () =>
        import(/* webpackChunkName: "districtManage" */ '@/views/serviceApp/addressManage/Index')
    },
    {
      path: 'managerChangeReview',
      name: 'ManagerChangeReview',
      meta: '客户经理变更审核',
      component: () =>
        import(/* webpackChunkName: "districtManage" */ '@/views/serviceApp/managerChangeReview/Index')
    }
  ]
}]
