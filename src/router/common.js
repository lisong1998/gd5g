/**
 * 公共路由
 */
const Layout = () => import(/* webpackChunkName: "Layout" */'@/components/layout/Layout')
export default [
  {
    path: '/',
    hidden: true,
    redirect: '/system'
  },
  {
    path: '/homepage',
    component: Layout,
    children: [{
      path: '/',
      name: 'Homepage',
      component: () => import(/* webpackChunkName: "homepage" */'@/views/Home')
    }]
  },
  {
    path: '*',
    name: '404',
    redirect: '/homepage',
    hidden: true
    // component: () => import(/* webpackChunkName: "404" */'@/views/About')
  }
]
