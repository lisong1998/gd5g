const tags = {
  state: {
    tags: [], // 打开的标签
    cache: []// （组件名）缓存的组件
  },
  mutations: {
    // 添加打开的标签,同步添加缓存
    ADD_TAG(state, view) {
      let exist = false
      for (let tag of state.tags) {
        if (tag.path === view.path) {
          tag.fullPath = view.fullPath
          exist = true
        }
      }
      if (!exist) {
        state.tags.push(
          Object.assign({}, view)
        )
      }
      if (state.cache.some(v => v === view.name)) { } else {
        // 设置定时让缓存在组件的beforeCreate后执行，解决第一次缓存无效问题
        setTimeout(() => {
          state.cache.push(view.name)
        }, 300)
      }
    },
    // 删除打开的标签,同步删除缓存
    DEL_TAG(state, view) {
      for (const [i, v] of state.tags.entries()) {
        if (v.path === view.path) {
          state.tags.splice(i, 1)
          break
        }
      }
      for (const [i, v] of state.cache.entries()) {
        if (v === view.name) {
          state.cache.splice(i, 1)
          break
        }
      }
    },
    // 删除缓存
    DEL_CACHE(state, name) {
      for (const [i, v] of state.cache.entries()) {
        if (v === name) {
          state.cache.splice(i, 1)
          break
        }
      }
    },
    SORT_TAGS(state, tags) {
      state.tags = tags
    }
  },
  actions: {
    addTag({ commit }, view) {
      commit('ADD_TAG', view)
    },
    delTag({ commit, state }, view) {
      return new Promise(resolve => {
        let index = 0
        for (const [i, v] of state.tags.entries()) {
          if (v.path === view.path) {
            index = i
          }
        }
        commit('DEL_TAG', view)
        let data = {
          tags: state.tags,
          index: index
        }
        resolve(data)
      })
    },
    delCache({ commit }, name) {
      commit('DEL_CACHE', name)
    },
    delOtherTags({ commit }, view) {
      commit('DEL_OTHER_TAGS', view)
    },
    delAllTags({ commit }) {
      commit('DEL_ALL_TAGS')
    },
    sortTags({ commit }, tags) {
      commit('SORT_TAGS', tags)
    }
  }
}

export default tags
